import React from 'react';
import PropTypes from 'prop-types';
import autoBind from 'react-autobind';
import { Link } from 'react-router-dom';
import { ButtonToolbar, ButtonGroup, Button, FormControl, FormGroup } from 'react-bootstrap';
import { withTracker } from 'meteor/react-meteor-data';
import { Meteor } from 'meteor/meteor';
import { Bert } from 'meteor/themeteorchef:bert';
import _ from 'lodash';

import Track from '../../../api/Track/Track';
import { TrackMostPopular } from '../../../api/Track/Track';
import TrackList from '../Track/TrackList';
import Loading from '../../misc/Loading/Loading';


class MostPopularTracks extends React.Component {
  constructor(props) {
    super(props);
    autoBind(this);
  }

  render() {
    const { loading, tracks, groupId, limit, countKey } = this.props;

    if (loading) return (<div className="MostPopularTracks"><Loading /></div>);

    // Have to sort tracks.
    const sortedTracks = _.sortBy(tracks, [track => -track[countKey]]).slice(0, Math.min(limit, tracks.length));

    return (
      <div className="MostPopularTracks">
        <h3>Most Popular Tracks</h3>

        { loading ? <Loading /> :
        <TrackList
          items={sortedTracks}
          noMenu={true}
          viewType="list"
          showMostRecentPlayList={true}
          preElements={{
            headers: ["Count"],
            elements: track => [
              <div className="popularity" key="popularity">{track[countKey]}</div>
            ],
          }}
        /> }
      </div>
    );
  }
}


export default withTracker(({ groupId }) => {
  const limit = 50;
  if (typeof groupId == 'undefined') {
    // TODO get group from logged in user or something.
    const group = Meteor.groups.findOne({name: "JD"});
    groupId = group._id;
  }
  else {
    // publication function only accepts strings or undefined, but allow passing truthy values to this component.
    groupId = groupId ? groupId : undefined;
  }

  const subscription = Meteor.subscribe('Track.mostPopular', limit, groupId);

  const countKey = 'playlistCount' + (groupId ? 'ForGroup-'+groupId : '');

  return {
    loading: !subscription.ready(),
    tracks: TrackMostPopular.find({[countKey]: {$exists: true}}).fetch(),
    groupId,
    limit,
    countKey,
  };
})(MostPopularTracks);
