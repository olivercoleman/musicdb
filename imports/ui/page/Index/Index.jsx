import { Meteor } from 'meteor/meteor'
import React from 'react';
import { Button } from 'react-bootstrap';
import ReactMarkdown from 'react-markdown';
import MediaQuery from 'react-responsive';

import Group from '../../music/Group/Group';
import Expandable from '../../misc/Expand/Expandable';

import './Index.scss';

const Index = () => {
  const header = (<div className="index-header">
    <h1>{Meteor.settings.public.appName}</h1>
    <ReactMarkdown className="description" source={Meteor.settings.public.appDescription} />
  </div>);

  return (
    <div className="Index">
      <Expandable collapsedHeight="2em" disabled={viewportDimensions => viewportDimensions.width >= Meteor.settings.public.smallScreenWidth}>
        {header}
      </Expandable>

      <div className="previous-set-lists">
        <h3>Previous Play Lists</h3>
        <Group group={Meteor.groups.findOne({name: "JD"})} />
      </div>
    </div>
  );
}

export default Index;
