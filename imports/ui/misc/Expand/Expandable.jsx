import React from 'react';
import autoBind from 'react-autobind';
import classNames from 'classnames';

import ExpandControl from './ExpandControl';

import './Expand.scss';

class Expandable extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      expanded: this.props.defaultExpanded || false,
      viewportDimensions: {
        width: -1,
        height: -1,
      },
      thisWidth: -1,
    };
    autoBind(this);
  }

  componentDidMount() {
    if (typeof this.props.disabled == 'function') {
      this.updateDims();
      window.addEventListener('resize', this.updateDims);
    }
  }

  updateDims() {
    this.setState({
      viewportDimensions: {
        width: Math.max(document.documentElement.clientWidth, window.innerWidth || 0),
        height: Math.max(document.documentElement.clientHeight, window.innerHeight || 0),
      },
      thisWidth: this.ref ? this.ref.offsetWidth : 0,
    });
  }

  render() {
    const { children, onToggle, collapsedHeight, collapsedWidth, expandedWidth,
      showEllipsis, disabled, className, contentClassName } = this.props;
    const { expanded, viewportDimensions, thisWidth } = this.state;

    const isDisabled = typeof disabled == 'function' ? !!disabled(viewportDimensions, thisWidth) : !!disabled;
    const isExpanded = isDisabled || !!expanded;
    const singleChild = React.Children.count(children) == 1;
    const childStyle = singleChild && children.props.style ? {...children.props.style} : {};
    childStyle.width = isExpanded ? (expandedWidth || "100%") : (collapsedWidth || "auto");

    return (
      <div
        className={classNames("Expandable", isDisabled ? "disabled" : "enabled", isExpanded ? 'expanded' : 'collapsed', className)}
        style={{height: isExpanded ? 'auto' : (collapsedHeight || '1.5em')}}
        ref={el => this.ref=el}
      >
        { !isDisabled && <ExpandControl expanded={isExpanded}
          onToggle={expanded => { this.setState({expanded}); onToggle && onToggle(expanded); }}
        /> }

        { singleChild ?
          React.cloneElement(this.props.children, {style: childStyle})
          :
          <div className={contentClassName || "expandable-content"} style={childStyle}>
            { children }
          </div>
        }

        { showEllipsis && <span className="ellipsis">…</span> }
      </div>
    )
  }
}

export default Expandable;
