import { Meteor } from 'meteor/meteor';
import { check, Match } from 'meteor/check';
import Artist from '../Artist';

Meteor.publish('Artist', function selector(selector) {
  check(selector, Match.OneOf(String, Object));
  return Artist.find(selector);
});


Meteor.publish('Artist.all', function all() {
  return Artist.find();
});

Meteor.publish('Artist.withId', function withId(documentId) {
  check(documentId, String);
  return Artist.find({ _id: documentId });
});
