import { Meteor } from 'meteor/meteor';
import { check, Match } from 'meteor/check';
import PlayList from '../../PlayList/PlayList';
import Track from '../Track';
import _ from 'lodash';


Meteor.publish('Track.all', function all() {
  return Track.find();
});

Meteor.publish('Track.withId', function withId(trackId) {
  check(trackId, String);
  return Track.find({ _id: trackId });
});

Meteor.publish('Track.playLists', function playLists(trackIds, groupId) {
  check(trackIds, [String]);
  check(groupId,  String);
  return PlayList.find({ trackIds: {$in: trackIds}, groupId });
});

Meteor.publish('Track.mostPopular', function playLists(limit, groupId) {
  // TODO: this is currently a non-reactive publication, make it reactive?
  check(limit, Number);
  check(groupId, Match.Maybe(String));

  const countKey = 'playlistCount' + (groupId ? 'ForGroup-'+groupId : '');

  const filters = {
    // Only include tracks that appear in 2 or more lists.
    'appearsInPlayLists.1': {$exists: true}
  }
  if (groupId) filters.appearsInPlayListGroups = groupId;
  const allTracks = Track.find(filters).fetch();

  let groupPlayListIds;
  if (groupId) groupPlayListIds = PlayList.find({groupId}, {fields: {_id: 1}}).map(pl => pl._id);

  // Add count key to tracks.
  for (let track of allTracks) {
    if (groupId)
      track[countKey] = _.intersection(track.appearsInPlayLists, groupPlayListIds).length;
    else
      track[countKey] = track.appearsInPlayLists.length;
  }

  const sortedTracks = _.sortBy(allTracks, [track => -track[countKey]]);

  // Add (initial) set of results within limit.
  for (let i = 0; i < Math.min(limit, sortedTracks.length); i++) {
    let track = sortedTracks[i];
    // Ignore tracks with less than two appearances in the specified group if necessary.
    if (track[countKey] < 2) break;
    this.added('TrackMostPopular', track._id, track);
  }

  this.ready();
});
